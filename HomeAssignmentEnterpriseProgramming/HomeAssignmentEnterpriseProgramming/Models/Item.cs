﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HomeAssignmentEnterpriseProgramming.Models
{
    public enum QualityEnum {
        Excellent,
        Good,
        Poor,
        Bad
    }
    public class Item
    {
        [Required]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ItemID { get; set; }

        [Required]
        [Display(Name = "Item Type")]
        public int ItemTypeID { get; set; }

        public ItemType ItemType { get; set; }
      
        public ApplicationUser Owner { get; set; }

        public string ApplicationUserId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "The quantity must be 1 or more!")]
        public int Quantity { get; set; }

        [Required]
        public QualityEnum Quality { get; set; }

        [Required]
        [Display(Name="Date Added")]
        public DateTime DateAdded { get; set; }

        [Required]
        [Range(0.0, (double)decimal.MaxValue, ErrorMessage = "The price must be 0 or more!")]
        public decimal Price { get; set; }
    }
}
