﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HomeAssignmentEnterpriseProgramming.Models
{
    public class ItemType
    {

        public ItemType()
        {
            this.Items = new HashSet<Item>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Reference { get; set; }

        //[Required]
        [Display(Name = "Category ID")]
        public int CategoryID { get; set; }

        public Category Category { get; set; }

        [Required]
        [Display(Name = "Item Type Name")]
        public String Name { get; set; }

        [Display(Name = "Upload Image")]
        public String ImageURL { get; set; }

        [Display(Name = "Item Type")]
        public virtual ICollection<Item> Items { get; set; }

    }
}