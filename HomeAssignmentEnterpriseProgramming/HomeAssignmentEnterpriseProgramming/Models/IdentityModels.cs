﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Dropbox.Api.TeamLog;
using Dropbox.Api.Users;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace HomeAssignmentEnterpriseProgramming.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        [Display(Name = "Owner")]
        public virtual ICollection<Item> Items { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("RetailConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer(new RetailDbContextInitializer());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<HomeAssignmentEnterpriseProgramming.Models.Category> Categories { get; set; }

        public System.Data.Entity.DbSet<HomeAssignmentEnterpriseProgramming.Models.Item> Items { get; set; }

        public System.Data.Entity.DbSet<HomeAssignmentEnterpriseProgramming.Models.ItemType> ItemTypes { get; set; }
    }

    public class RetailDbContextInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            base.Seed(context);
            
            Console.WriteLine("Seeding..");

            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            roleManager.Create(new IdentityRole() { Name = "RegisteredUser" });

            for (int u = 1; u < 5; u++)
            {
                ApplicationUser user = new ApplicationUser()
                {
                    Email = "user" + u + "@gmail.com",
                    UserName = "user" + u + "@gmail.com"
                };

                userManager.Create(user, "P@ssw0rd");
                userManager.AddToRole(user.Id, "RegisteredUser");
            }

            //generate categories
            context.Categories.Add(new Category() { CategoryName = "Clothing" });
            context.Categories.Add(new Category() { CategoryName = "Furniture" });
            context.Categories.Add(new Category() { CategoryName = "Technology" });
            context.Categories.Add(new Category() { CategoryName = "Property" });

            //generate item types
            Random randomItemType = new Random(73);
            foreach (Category category in context.Categories.Local)
            {
                for (int it = 0; it < 20; it++)
                {
                    ItemType itemType = new ItemType()
                    {
                        Category = category,
                        CategoryID = category.CategoryID,
                        Name = "Type #" + randomItemType.Next(1, 999),
                        ImageURL = "https://via.placeholder.com/150"
                    };

                    context.ItemTypes.Add(itemType);
                }
            }

            //generate items
            Random price = new Random(42);
            Random quantity = new Random(64);
            Random member = new Random(113);

            var users = context.Users.Local;
            foreach (ItemType itemType in context.ItemTypes.Local)
            {
                decimal baseprice = ((decimal)price.Next(1, 100000)) / 100;
                foreach (QualityEnum quality in Enum.GetValues(typeof(QualityEnum)))
                {
                    Item item = new Item()
                    {
                        ItemTypeID = itemType.Reference,
                        ItemType = itemType,
                        Quality = quality,
                        Quantity = quantity.Next(1, 10),
                        Price = baseprice,
                        DateAdded = DateTime.Now
                    };

                    ApplicationUser user = users[member.Next(0, users.Count)];
                    item.Owner = user;
                    item.ApplicationUserId = user.Id;
                    context.Items.Add(item);
                    baseprice = baseprice * (decimal)0.5;
                   
                }
            }
        }
    }
}