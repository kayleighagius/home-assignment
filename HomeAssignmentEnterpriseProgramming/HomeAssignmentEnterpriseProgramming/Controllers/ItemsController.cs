﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using HomeAssignmentEnterpriseProgramming.Models;

namespace HomeAssignmentEnterpriseProgramming.Controllers
{
    public class ItemsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Items
        [AllowAnonymous]
        public ActionResult Index(string search, int? page, string sortBy)
        {
            ViewBag.SortDateParameter = string.IsNullOrEmpty(sortBy) ? "Newest First" : "";

            var items = db.Items.AsQueryable().Include(i => i.ItemType);
            items = items.Include(i=>i.Owner);
            items = items.Where(x=>x.Owner.UserName.StartsWith(search) || search==null);

            switch (sortBy)
            {
                case "Newest First": items = items.OrderByDescending(x => x.DateAdded);
                    break;
                default: items = items.OrderBy(x=>x.DateAdded);
                    break;
            }

            return View(items.ToList().ToPagedList(page ?? 1, 5));
        }

        public ActionResult ItemPartialView(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Item item = db.Items.Find(id);
            item.Owner = db.Users.FirstOrDefault(i => i.Id == item.ApplicationUserId);
            item.ItemType = db.ItemTypes.FirstOrDefault(i => i.Reference == item.ItemTypeID);

            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // GET: Items/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Item item = db.Items.Find(id);
            item.Owner = db.Users.FirstOrDefault(i => i.Id == item.ApplicationUserId);
            item.ItemType = db.ItemTypes.FirstOrDefault(i => i.Reference == item.ItemTypeID);

            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // GET: Items/Create
        [Authorize()]

        public ActionResult Create()
        {
            ViewBag.ItemTypeID = new SelectList(db.ItemTypes, "Reference", "Name");
            return View();
        }

        // POST: Items/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize()]
        public ActionResult Create([Bind(Include = "ItemID,ItemTypeID,Owner,ApplicationUserId,Quantity,Quality,Price,DateAdded")] Item item)
        {
            var owner = db.Users.FirstOrDefault(u=>u.UserName==User.Identity.Name);
            item.Owner = owner;
            item.ApplicationUserId = owner.Id;
            item.Owner.Items.Add(item);
            item.DateAdded = DateTime.Now;

            if (ModelState.IsValid)
            {
                if (db.Items.SingleOrDefault(x=> x.ItemTypeID == item.ItemTypeID && x.ApplicationUserId == item.ApplicationUserId && x.Quality == item.Quality && x.Price == item.Price) == null)
                {
                    item.ItemType = db.ItemTypes.FirstOrDefault(i => i.Reference == item.ItemTypeID);
                    item.ItemType.Items.Add(item);
                    db.Items.Add(item);

                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Error = "You've already put this item on sale! Try adding more to its quantity by editing it.";
                }
            }

            ViewBag.ItemTypeID = new SelectList(db.ItemTypes, "Reference", "Name", item.ItemTypeID);
            return View(item);

        }

        // GET: Items/Edit/5
        [Authorize()]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Item item = db.Items.Find(id);
            item.Owner = db.Users.FirstOrDefault(i => i.Id == item.ApplicationUserId);

            if (User.Identity.Name == item.Owner.UserName)
            {
                if (item == null)
                {
                    return HttpNotFound();
                }

                ViewBag.ItemTypeID = new SelectList(db.ItemTypes, "Reference", "Name");

                return View(item);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        // POST: Items/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize()]
        public ActionResult Edit([Bind(Include = "ItemID,ItemTypeID,Owner,ApplicationUserId,Quantity,Quality,Price,DateAdded")] Item item)
        {
            var owner = db.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);
            item.Owner = owner;
            item.ApplicationUserId = owner.Id;
            item.Owner.Items.Add(item);
            item.DateAdded = DateTime.Now;

            if (ModelState.IsValid)
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ItemTypeID = new SelectList(db.ItemTypes, "Reference", "Name", item.ItemTypeID);

            return View(item);
        }

        // GET: Items/Delete/5
        [Authorize()]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.Items.Find(id);
            item.Owner = db.Users.FirstOrDefault(i => i.Id == item.ApplicationUserId);

            if (User.Identity.Name == item.Owner.UserName)
            {
                if (item == null)
                {
                    return HttpNotFound();
                }
                return View(item);
            }
            else
            {
                ViewBag.Error = "That doesn't belong to you!";
                return RedirectToAction("Index");
            }
        }

        // POST: Items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize()]
        public ActionResult DeleteConfirmed(int id)
        {
            Item item = db.Items.Find(id);
            db.Items.Remove(item);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
