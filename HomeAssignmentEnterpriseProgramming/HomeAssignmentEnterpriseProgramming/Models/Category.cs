﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Policy;
using System.Web;

namespace HomeAssignmentEnterpriseProgramming.Models
{
    public class Category
    {
        public Category()
        {
            this.ItemTypes = new HashSet<ItemType>();
        }

        [Required]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CategoryID { get; set; }

        [Required]
        [Display(Name = "Category")]
        public String CategoryName { get; set; }

        [Display(Name = "Category")]
        public virtual ICollection<ItemType> ItemTypes { get; set; }
    } 
}