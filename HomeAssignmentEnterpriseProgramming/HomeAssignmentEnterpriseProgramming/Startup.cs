﻿using HomeAssignmentEnterpriseProgramming.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using System;

[assembly: OwinStartupAttribute(typeof(HomeAssignmentEnterpriseProgramming.Startup))]
namespace HomeAssignmentEnterpriseProgramming
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesAndDefaultUsers();
        }

        private void createRolesAndDefaultUsers()
        {
            using (ApplicationDbContext context = new ApplicationDbContext())
            {
                using (RoleManager<IdentityRole> roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context)))
                {
                    //Checks if Admin role exists, if it does, do nothing
                    if (!roleManager.RoleExists("Admin"))
                    {
                        IdentityRole role = new IdentityRole();
                        role.Name = "Admin";
                        roleManager.Create(role);
                    }
                            
                    //Checks if registerd use role exists, if it does, do nothing
                    if (!roleManager.RoleExists("RegisteredUser"))
                    {
                        IdentityRole role = new IdentityRole();
                        role.Name = "RegisteredUser";
                        roleManager.Create(role);
                    }
                       
                }

                using (UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context)))
                {
                    //admin user check
                    if(userManager.FindByEmail("admin_email@live.com") == null)
                    {
                        ApplicationUser user = new ApplicationUser();
                        user.UserName = "admin_email@live.com";
                        user.Email = "admin_email@live.com";

                        string userPass = "P@ssw0rd_1234";

                        IdentityResult check = userManager.Create(user, userPass);

                        if (check.Succeeded)
                        {
                            IdentityResult checkRole = userManager.AddToRole(user.Id, "Admin");

                            if (!checkRole.Succeeded)
                            {
                                Console.Error.WriteLine("Adminstrator was not assigned the role of Admin successfully.");
                                Console.WriteLine("Adminstrator was not assigned the role of Admin successfully.");
                            }
                        }
                        else
                        {
                            Console.Error.WriteLine("The role of Admin was not created successfully.");
                            Console.WriteLine("The role of Admin was not created successfully.");
                        }
                    }
                }
            }
        }
    }
}
