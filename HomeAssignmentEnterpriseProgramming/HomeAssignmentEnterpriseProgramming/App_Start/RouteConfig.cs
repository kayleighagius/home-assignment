﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HomeAssignmentEnterpriseProgramming
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "CategoriesIndex",
                url: "Categories/{action}/{id}",
                defaults: new { controller = "Categories", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ItemTypesIndex",
                url: "ItemTypes/{action}/{id}",
                defaults: new { controller = "ItemTypes", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ItemsIndex",
                url: "Items/{action}/{id}",
                defaults: new { controller = "Items", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
