﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dropbox.Api;
using Dropbox.Api.Files;
using HomeAssignmentEnterpriseProgramming.Models;

namespace HomeAssignmentEnterpriseProgramming.Controllers
{
    public class ItemTypesController : Controller
    {

        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ItemTypes
        [AllowAnonymous]
        public ActionResult Index()
        {
            var itemTypes = db.ItemTypes.Include(it => it.Category);
            return View(itemTypes.ToList());
        }

        // GET: ItemTypes/Details/5
        [AllowAnonymous]
        [Authorize(Roles = "RegisteredUser, Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ItemType itemType = db.ItemTypes.Find(id);
            itemType.Category = db.Categories.FirstOrDefault(it => it.CategoryID==itemType.CategoryID);
            if (itemType == null)
            {
                return HttpNotFound();
            }
            return View(itemType);
        }

        // GET: ItemTypes/Create
        [Authorize(Roles = "RegisteredUser, Admin")]
        public ActionResult Create()
        {
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName");
            return View();
        }

        // POST: ItemTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "RegisteredUser, Admin")]
        public ActionResult Create([Bind(Include = "Reference,CategoryID,Name,ImageURL")] ItemType itemType, HttpPostedFileBase file)
        {

            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    if ((Path.GetExtension(file.FileName) == ".jpeg") || (Path.GetExtension(file.FileName) == ".jpg") || (Path.GetExtension(file.FileName) == ".png"))
                    {
                        bool check = false;

                        byte[] bytesToCheck = new byte[4];

                        file.InputStream.Position = 0;
                        file.InputStream.Read(bytesToCheck, 0, 4);

                        //jpg/jpeg


                        if (bytesToCheck[0] == 255
                            && bytesToCheck[1] == 216
                            && bytesToCheck[2] == 255
                            && bytesToCheck[3] == 224)
                        {
                            check = true;
                        }

                        //png
                        if (check == false)
                        {
                            bytesToCheck = new byte[4];

                            file.InputStream.Position = 0;
                            file.InputStream.Read(bytesToCheck, 0, 4);

                            if (bytesToCheck[0] == 137
                                    && bytesToCheck[1] == 80
                                    && bytesToCheck[2] == 78
                                    && bytesToCheck[3] == 71)
                            {
                                check = true;
                            }
                        }

                        if (check == true)
                        {
                            itemType.ImageURL = UploadImageToDropbox(file);

                            db.ItemTypes.Add(itemType);
                            db.SaveChanges();

                            return RedirectToAction("Index");
                        }
                    }
                }
                else
                {
                    ViewBag.Error("Image must be uploaded!");
                }
            }
            ModelState.Clear();

            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", itemType.CategoryID);
            return View(itemType);
        }

        private string UploadImageToDropbox(HttpPostedFileBase image)
        {
            string accessToken = "bnPbUOrZReEAAAAAAAAHKzQmEqerec-eaatuN1AgAA_kgwInslBIYUUhjJmxfBVi";
            string appName = "EnterpriseProgrammingUpload";
            string imageURL = string.Empty;

            using(DropboxClient client = new DropboxClient(accessToken, new DropboxClientConfig(appName)))
            {
                string originalExt = Path.GetExtension(image.FileName);
                string originalFileName = Path.GetFileNameWithoutExtension(image.FileName);

                //string[] inputFileName = image.FileName.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);
                //string fileNameAndExt = inputFileName[inputFileName.Length - 1];

                //string[] fileNameAndExtSplit = fileNameAndExt.Split('.');
                //string originalFileName = fileNameAndExtSplit[0];
                //string originalExt = fileNameAndExtSplit[1];

                string fileName = @"/Images/" + originalFileName + Guid.NewGuid().ToString().Replace("-", "") + originalExt;

                var updated = client.Files.UploadAsync(
                    fileName,
                    mode: WriteMode.Overwrite.Overwrite.Instance,
                    body: image.InputStream
                ).Result;

                var result = client.Sharing.CreateSharedLinkWithSettingsAsync(fileName).Result;
            
                imageURL = result.Url.Replace("dl=0", "raw=1");
            }

            return imageURL;
        }

        // GET: ItemTypes/Edit/5
        [Authorize(Roles = "RegisteredUser, Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemType itemType = db.ItemTypes.Find(id);
            if (itemType == null)
            {
                return HttpNotFound();
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName");

            return View(itemType);
        }

        // POST: ItemTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "RegisteredUser, Admin")]
        public ActionResult Edit([Bind(Include = "Reference,CategoryID,Name,ImageURL")] ItemType itemType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(itemType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", itemType.CategoryID);

            return View(itemType);
        }

        // GET: ItemTypes/Delete/5
        [Authorize(Roles = "RegisteredUser, Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemType itemType = db.ItemTypes.Find(id);
            if (itemType == null)
            {
                return HttpNotFound();
            }
            return View(itemType);
        }

        // POST: ItemTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "RegisteredUser, Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            ItemType itemType = db.ItemTypes.Find(id);
            if (itemType.Items.Count == 0)
            {
                db.ItemTypes.Remove(itemType);

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Details/"+itemType.Reference);
            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
